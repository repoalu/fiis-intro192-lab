def imprimir(matriz, filas, columnas):
    for i in range(filas):
        for j in range(columnas):
            print(matriz[i][j], end = " ")
        print()
            
#Pruebas
matriz = [ [1, 2, 3],    
           [4, 5, 6],
           [7, 8, 9],
           [10, 11, 12],
         ]
imprimir(matriz, 4, 3)
