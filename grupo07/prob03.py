def mostrar_estadisticas(ventas, cant_prod, cant_trim):
    for i in range(cant_prod):
        suma = 0
        for j in range(cant_trim):
            suma = suma + ventas[i][j]
        print("Ventas prod", i + 1, ":", suma)
    
    for j in range(cant_trim):
        suma = 0
        for i in range(cant_prod):
            suma = suma + ventas[i][j]
        print("Ventas trimestre", j + 1, ":", suma)

def main():
    lista = [   [10, 20, 14, 34],
                [15, 35, 66, 8],
                [12, 10, 26, 11]
            ]
    mostrar_estadisticas(lista, len(lista), len(lista[0]))

#Prueba
main()
