def es_simetrica(matriz, orden):
    simetrica = True
    for i in range(orden):
        for j in range(orden):
            if(matriz[i][j] != matriz[j][i]):
                simetrica = False
    return simetrica

def main():
    matriz = [ [1, 0, 1],
               [0, 1, 1],
               [1, 1, 1]
             ]
    respuesta = es_simetrica(matriz, len(matriz))
    if(respuesta == True):
        print("Matriz simetrica")
    else:
        print("Matriz no es simetrica")

#Prueba
main()

